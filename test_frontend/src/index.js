import React from "react";
import ReactDOM from "react-dom";
import {
  Route,
  Link,
  HashRouter as Router,
  Routes,
  Navigate,
} from "react-router-dom";
import { Navbar, Nav, Button, Container } from "react-bootstrap";
import { logout } from "./services/auth";
import Login from "./components/Login/Login";
import AddWine from "./components/front-end-classes/AddWine";
import Wine from "./components/front-end-classes/Wine";

class App extends React.Component {
  render() {
    const jwt = window.localStorage["jwt"];
    if (jwt) {
      return (
        <>
          <Router>
            <Navbar expand bg="dark" variant="dark">
              <Navbar.Brand as={Link} to="/">
                JWD
              </Navbar.Brand>
              {window.localStorage["role"] == "ROLE_ADMIN" ||
              window.localStorage["role"] == "ROLE_KORISNIK" ? (
                <Nav>
                  <Nav.Link as={Link} to="/wines">
                    Wines
                  </Nav.Link>
                </Nav>
              ) : null}

              {window.localStorage["jwt"] ? (
                <Button onClick={() => logout()}>Log out</Button>
              ) : (
                <Nav.Link as={Link} to="/login">
                  Log in
                </Nav.Link>
              )}
            </Navbar>
            <Container style={{ paddingTop: "10px" }}>
              <Routes>
                <Route path="/login" element={<Login />} />
                <Route path="/wines" element={<Wine />} />
                <Route path="/wines/add" element={<AddWine />} />
              </Routes>
            </Container>
          </Router>
        </>
      );
    } else {
      return (
        <>
          <Router>
            <Navbar expand bg="dark" variant="dark">
              <Navbar.Brand as={Link} to="/">
                JWD
              </Navbar.Brand>
              <Nav>
                <Nav.Link as={Link} to="/login">
                  Login
                </Nav.Link>
              </Nav>
            </Navbar>
            <Container style={{ paddingTop: "10px" }}>
              <Routes>
                <Route path="/login" element={<Login />} />

                <Route path="*" element={<Navigate replace to="/login" />} />
              </Routes>
            </Container>
          </Router>
        </>
      );
    }
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));

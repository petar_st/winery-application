import React from "react";
import { Row, Col, Button, Table, Form } from "react-bootstrap";
import Axios from "../../apis/Axios";
import { withParams, withNavigation } from "../../routeconf";

class Entity extends React.Component {
  constructor(props) {
    super(props);

    let search = {
      wineryId: "",
      wineName: "",
      pageNo: 0,
    };

    this.state = {
      search: search,
      entity: [],
      winaries: [],
      numberOfBottlesToOder: "",
      numberOfBottlesToBuy: "",

      totalPages: 2,
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    this.getEntity(this.state.search.pageNo);
    this.getWinaries();
  }

  getEntity(newPageNo) {
    if (newPageNo < 0) {
      newPageNo = 0;
    }

    let search = this.state.search;
    let config = {
      params: {
        wineryId: search.wineryId,
        wineName: search.wineName,

        pageNo: newPageNo,
      },
    };
    Axios.get("/wines", config)
      .then((res) => {
        console.log(res);
        search.pageNo = newPageNo;
        this.setState({
          entity: res.data,
          search: search,
          totalPages: res.headers["total-pages"],
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getWinaries() {
    Axios.get("/wineries")
      .then((res) => {
        console.log(res);
        this.setState({ winaries: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  onOrderInputChange(event) {
    let numberOfBottlesToOder = event.target.value;
    this.setState({ numberOfBottlesToOder: numberOfBottlesToOder });
  }

  onBuyInputChange(event) {
    let numberOfBottlesToBuy = event.target.value;
    this.setState({ numberOfBottlesToBuy: numberOfBottlesToBuy });
  }

  onSearchInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let search = this.state.search;
    search[name] = value;
    console.log(search);
    this.setState({ search: search });
  }

  goToAdd() {
    this.props.navigate("/wines/add");
  }

  deleteWine(id) {
    Axios.delete("/wines/" + id)
      .then((res) => {
        console.log(res);
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderWines() {
    return this.state.entity.map((entity) => {
      return (
        <>
          {window.localStorage["role"] == "ROLE_USER" &&
          entity.brojDostupnihFlasa == 0 ? null : (
            <tr key={entity.id}>
              <td>{entity.name}</td>
              <td>{entity.wineryName}</td>
              <td>{entity.wineTypeName}</td>
              {window.localStorage["role"] == "ROLE_ADMIN" ? (
                <td>{entity.numberOfAvailableBottles}</td>
              ) : null}
              <td>{entity.year}</td>
              <td>{entity.pricePerBottle}</td>
              {window.localStorage["role"] == "ROLE_USER" ? (
                <td>{entity.description}</td>
              ) : null}
              {window.localStorage["role"] == "ROLE_ADMIN" &&
              entity.numberOfAvailableBottles < 10 ? (
                <>
                  <td>
                    <Form.Control
                      placeholder="order"
                      type="number"
                      min="0"
                      onChange={(e) => this.onOrderInputChange(e)}
                    ></Form.Control>
                  </td>
                  <td>
                    <Button onClick={() => this.GoToOrder(entity.id)}>
                      Add to Warehouse
                    </Button>
                  </td>
                </>
              ) : null}
              {window.localStorage["role"] == "ROLE_USER" ? (
                <>
                  <td>
                    <Form.Control
                      placeholder="buy"
                      type="number"
                      min="0"
                      onChange={(e) => this.onBuyInputChange(e)}
                    ></Form.Control>
                  </td>
                  <td>
                    <Button onClick={() => this.GoToBuy(entity.id)}>Buy</Button>
                  </td>
                </>
              ) : null}
              {window.localStorage["role"] == "ROLE_ADMIN" ? (
                <td>
                  <Button
                    onClick={() => this.deleteWine(entity.id)}
                    variant="danger"
                  >
                    Delete
                  </Button>
                </td>
              ) : null}
            </tr>
          )}{" "}
        </>
      );
    });
  }

  GoToOrder(id) {
    console.log(id);
    let numberOfBottlesToOder = this.state.numberOfBottlesToOder;
    let config = {
      params: {
        wineId: id,
        numberOfBottlesToOder: numberOfBottlesToOder,
      },
    };
    Axios.get("/wines/order", config)
      .then((res) => {
        console.log(res);
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  GoToBuy(id) {
    console.log(id);
    let numberOfBottlesToBuy = this.state.numberOfBottlesToBuy;

    let config = {
      params: {
        wineId: id,
        numberOfBottlesToBuy: numberOfBottlesToBuy,
      },
    };
    Axios.get("/wines/buy", config)
      .then((res) => {
        console.log(res);
        window.location.reload();
      })
      .catch((err) => {
        alert(
          "You tried to order more wine than available. Try to use some smaller amouont"
        );
        console.log(err);
      });
  }

  renderDropdown() {
    return this.state.winaries.map((e) => {
      return (
        <option value={e.id} key={e.id}>
          {e.name}
        </option>
      );
    });
  }

  renderSearchEntity() {
    return (
      <div className="search">
        <br />

        <Row>
          <Col>
            <Form.Group>
              <Form.Label>Winery</Form.Label>
              <Form.Select
                name="wineryId"
                as="input"
                type="number"
                onChange={(e) => this.onSearchInputChange(e)}
              >
                <option value="">Choose Winery</option>
                {this.renderDropdown()}
              </Form.Select>
            </Form.Group>
          </Col>
        </Row>
        <Form style={{ width: "100%" }}>
          <Row>
            <Col>
              <Form.Group>
                <Form.Label>Wine Name</Form.Label>
                <Form.Control
                  name="wineName"
                  as="input"
                  type="text"
                  placeholder="Wine name"
                  onChange={(e) => this.onSearchInputChange(e)}
                ></Form.Control>
              </Form.Group>
            </Col>
          </Row>
        </Form>
        <Row>
          <Col>
            <br />
            <Button onClick={() => this.getEntity(this.state.search.pageNo)}>
              Search
            </Button>
          </Col>
        </Row>
      </div>
    );
  }

  renderDisplayEntity() {
    return (
      <div className="prikazEntiteta">
        <h1>Prikaz Entiteta</h1>
        {window.localStorage["role"] == "ROLE_ADMIN" ? (
          <Button onClick={() => this.goToAdd()} variant="success">
            Add Wine
          </Button>
        ) : null}
        <Button
          disabled={this.state.search.pageNo == 0}
          onClick={() => this.getEntity(this.state.search.pageNo - 1)}
        >
          Previous
        </Button>
        <Button
          disabled={this.state.search.pageNo == this.state.totalPages - 1}
          onClick={() => this.getEntity(this.state.search.pageNo + 1)}
        >
          Next
        </Button>
        <Table>
          <thead>
            <tr>
              <th>Wine name</th>
              <th>Winery</th>
              <th>Wine Type</th>
              {window.localStorage["role"] == "ROLE_ADMIN" ? (
                <th>Number of Available Bottles</th>
              ) : null}
              <th>Year</th>
              <th>Price per Bottle</th>
              {window.localStorage["role"] == "ROLE_USER" ? (
                <th>Description</th>
              ) : null}
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderWines()}</tbody>
        </Table>
      </div>
    );
  }

  render() {
    return (
      <div>
        {window.localStorage["role"] == "ROLE_ADMIN" ||
        window.localStorage["role"] == "ROLE_USER" ? (
          <div>
            {this.renderSearchEntity()}
            {this.renderDisplayEntity()}
          </div>
        ) : null}
      </div>
    );
  }
}
export default withParams(withNavigation(Entity));

import React from "react";
import { Row, Col, Button, Form } from "react-bootstrap";
import Axios from "../../apis/Axios";
import { withNavigation } from "../../routeconf";

class addEntity extends React.Component {
  constructor(props) {
    super(props);

    let params = {
      name: "",
      description: "",
      year: "",
      pricePerBottle: "",
      wineTypeId: "",
      wineryId: "",
    };
    this.state = { params: params, winery: [], wineTypes: [] };
  }

  componentDidMount() {
    this.getDropdownEntities();
    this.getDropdownEntities2();
  }

  getDropdownEntities() {
    Axios.get("/wine-types")
      .then((res) => {
        console.log(res);
        this.setState({ winery: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getDropdownEntities2() {
    Axios.get("/wineries")
      .then((res) => {
        console.log(res);
        this.setState({ wineTypes: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  renderWineryDropdown() {
    return this.state.winery.map((e) => {
      return (
        <option value={e.id} key={e.id}>
          {e.name}
        </option>
      );
    });
  }

  renderWineTypesDropdown() {
    return this.state.wineTypes.map((e) => {
      return (
        <option value={e.id} key={e.id}>
          {e.name}
        </option>
      );
    });
  }

  onInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let params = this.state.params;
    params[name] = value;
    console.log(params);
    this.setState({ params: params });
  }

  createEntity(e) {
    let params = this.state.params;
    let dto = {
      name: params.name,
      description: params.description,
      year: params.year,
      pricePerBottle: params.pricePerBottle,
      wineTypeId: params.wineTypeId,
      wineryId: params.wineryId,
    };
    console.log("dto: " + JSON.stringify(dto));
    try {
      Axios.post("/wines", dto).then((res) => {
        console.log(res);
        this.props.navigate("/wines");
      });
    } catch (err) {
      alert("Someething went wrong. Please check the data that you added");
      console.log(err);
    }
  }

  render() {
    return (
      <>
        <div>
          {window.localStorage["role"] == "ROLE_ADMIN" ? (
            <div>
              <Form style={{ width: "100%" }}>
                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Wine name</Form.Label>
                      <Form.Control
                        name="name"
                        as="input"
                        type="text"
                        placeholder="Wine name"
                        onChange={(e) => this.onInputChange(e)}
                      ></Form.Control>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Wine description</Form.Label>
                      <Form.Control
                        name="description"
                        as="input"
                        type="text"
                        placeholder="Wine description"
                        onChange={(e) => this.onInputChange(e)}
                      ></Form.Control>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Year of Production</Form.Label>
                      <Form.Control
                        name="year"
                        as="input"
                        type="number"
                        min="0"
                        placeholder="Year of Production"
                        onChange={(e) => this.onInputChange(e)}
                      ></Form.Control>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Price per Bottle</Form.Label>
                      <Form.Control
                        name="pricePerBottle"
                        as="input"
                        type="number"
                        min="0"
                        placeholder="Price per bottle"
                        onChange={(e) => this.onInputChange(e)}
                      ></Form.Control>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Wine Type</Form.Label>
                      <Form.Select
                        name="wineTypeId"
                        onChange={(e) => this.onInputChange(e)}
                      >
                        <option>Choose Vine Type</option>
                        {this.renderWineryDropdown()}
                      </Form.Select>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Winary</Form.Label>
                      <Form.Select
                        name="wineryId"
                        onChange={(e) => this.onInputChange(e)}
                      >
                        <option>Choose Winary</option>
                        {this.renderWineTypesDropdown()}
                      </Form.Select>
                    </Form.Group>
                  </Col>
                </Row>
              </Form>
              <br />
              <Row>
                <Col>
                  <Button onClick={(e) => this.createEntity(e)}>
                    Create Wine
                  </Button>
                </Col>
              </Row>
            </div>
          ) : null}
        </div>
      </>
    );
  }
}

export default withNavigation(addEntity);

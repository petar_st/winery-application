INSERT INTO user (id, e_mail, username, password, first_name, last_name, role)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO user (id, e_mail, username, password, first_name, last_name, role)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','USER');
INSERT INTO user (id, e_mail, username, password, first_name, last_name, role)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','USER');
              
      
INSERT INTO wine_type(name) VALUES ('red');
INSERT INTO wine_type(name) VALUES ('white');
INSERT INTO wine_type(name) VALUES ('sweet');

INSERT INTO winery (year_of_establishment, name) VALUES (1956, 'Winery of Great wines!');
INSERT INTO winery (year_of_establishment, name) VALUES (1986, 'Traditional Winery from heart of Serbia');
--
INSERT INTO wine (number_of_available_bottles, price_per_bottle, year, name, description, wine_type_id, winery_id) VALUES (9, 1250, 2011, 'Wine ABC!', 'Fantastic!', 1, 1);
INSERT INTO wine (number_of_available_bottles, price_per_bottle, year, name, description, wine_type_id, winery_id) VALUES (80, 800, 2018, 'Juicy Wine', 'Awesome!', 2, 1);
INSERT INTO wine (number_of_available_bottles, price_per_bottle, year, name, description, wine_type_id, winery_id) VALUES (30, 300, 2015, 'Fantastic-o Wine-o', '10/10', 3, 1);
INSERT INTO wine (number_of_available_bottles, price_per_bottle, year, name, description, wine_type_id, winery_id) VALUES (20, 750, 2013, 'Fantasy Wine', 'Sweet', 2, 2);
INSERT INTO wine (number_of_available_bottles, price_per_bottle, year, name, description, wine_type_id, winery_id) VALUES (80, 1100, 2012, 'The Good Old Wine', 'Sour', 3, 2);
INSERT INTO wine (number_of_available_bottles, price_per_bottle, year, name, description, wine_type_id, winery_id) VALUES (120, 860, 2021, 'The Last Wine', 'No Hangover', 1, 2);
package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Wine;
import com.ftninformatika.jwd.modul3.test.web.dto.WineDTO;

@Component
public class WineToWineDTO implements Converter<Wine, WineDTO> {

    @Override
    public WineDTO convert(Wine source) {
        WineDTO eDto = new WineDTO();

        eDto.setId(source.getId());
        eDto.setNumberOfAvailableBottles(source.getNumberOfAvailableBottles());
        eDto.setPricePerBottle(source.getPricePerBottle());
        eDto.setYear(source.getYear());
        eDto.setName(source.getName());
        eDto.setWineryId(source.getWinery().getId());
        eDto.setWineryName(source.getWinery().getName());
        eDto.setWineTypeId(source.getWineType().getId());
        eDto.setWineTypeName(source.getWineType().getName());
        eDto.setWineryId(source.getWinery().getId());
        eDto.setWineryName(source.getWinery().getName());
        eDto.setDescription(source.getDescription());

        return eDto;

    }

    public List<WineDTO> convert(List<Wine> entities) {
        List<WineDTO> entityDTOs = new ArrayList<>();

        for (Wine e : entities) {
            entityDTOs.add(convert(e));
        }

        return entityDTOs;
    }

}
package com.ftninformatika.jwd.modul3.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.modul3.test.model.Wine;

@Repository
public interface WineRepository extends JpaRepository<Wine, Long> {

    Wine findOneById(Long id);

    Page<Wine> findByNameIgnoreCaseContains(String name, Pageable p);

    Page<Wine> findByWineryIdAndNameIgnoreCaseContains(Long wineryId, String name, Pageable of);
}

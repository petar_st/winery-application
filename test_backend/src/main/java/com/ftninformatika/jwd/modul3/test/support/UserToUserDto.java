package com.ftninformatika.jwd.modul3.test.support;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.User;
import com.ftninformatika.jwd.modul3.test.web.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserToUserDto implements Converter<User, UserDTO>{

    @Autowired
    private AddressToAddressDto toAddressDto;

    @Override
    public UserDTO convert(User user) {
        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setAddressDTO(toAddressDto.convert(user.getAddress()));
        userDTO.seteMail(user.geteMail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastNAme(user.getLastName());
        userDTO.setUsername(user.getUsername());

        return userDTO;
    }

    public List<UserDTO> convert(List<User> users){
        List<UserDTO> userDTOS = new ArrayList<>();

        for(User k : users) {
            UserDTO dto = convert(k);
            userDTOS.add(dto);
        }

        return userDTOS;
    }
}

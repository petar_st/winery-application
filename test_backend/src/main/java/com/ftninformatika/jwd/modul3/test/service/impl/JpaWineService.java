package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import com.ftninformatika.jwd.modul3.test.model.Wine;
import com.ftninformatika.jwd.modul3.test.repository.WineRepository;
import com.ftninformatika.jwd.modul3.test.service.WineService;


@Service
public class JpaWineService implements WineService {

@Autowired
private WineRepository wineRepository;

	public List<Wine> findAll() {
		return wineRepository.findAll();
	}

	@Override
	public Wine findOneById(Long id) {
		return wineRepository.findOneById(id);
	}

	@Override
	public Wine save(Wine competitor) {
		return wineRepository.save(competitor);
	}

	@Override
	public Wine update(Wine competitor) {
		return wineRepository.save(competitor);
	}

	@Override
	public Wine delete(Long id) {
		Wine e = wineRepository.findOneById(id);
		wineRepository.deleteById(id);

		return null;
	}

	@Override
	public Page<Wine> find(Long wineryId, String name, Integer pageNo) {
		if (name == null) {
			name = "";
		}
		
		if (wineryId == null) {
			return wineRepository.findByNameIgnoreCaseContains(name, PageRequest.of(pageNo, 2));
		}
		
		return wineRepository.findByWineryIdAndNameIgnoreCaseContains(wineryId, name, PageRequest.of(pageNo, 2));
	}

	@Override
	public Void processAddToWarehouse(Long wineId, int numberOfBottles) {
		Wine wine = findOneById(wineId);

		wine.setNumberOfAvailableBottles(wine.getNumberOfAvailableBottles() + numberOfBottles);
		save(wine);
		return null;
	}

	@Override
	public Integer processPurchase(Long wineId, int numberOfBottles) {
		Wine wine = findOneById(wineId);
		
		if (wine.getNumberOfAvailableBottles() - numberOfBottles < 0) {
			return 0;
		}

		wine.setNumberOfAvailableBottles(wine.getNumberOfAvailableBottles() - numberOfBottles);
		save(wine);
		return 1;
	}

}

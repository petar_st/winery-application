package com.ftninformatika.jwd.modul3.test.web.dto;


public class WineTypeDTO {
    private Long id;

    private String name;

    public WineTypeDTO() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

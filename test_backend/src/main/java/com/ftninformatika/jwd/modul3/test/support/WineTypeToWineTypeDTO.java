package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.WineType;
import com.ftninformatika.jwd.modul3.test.web.dto.WineTypeDTO;

@Component
public class WineTypeToWineTypeDTO implements Converter<WineType, WineTypeDTO> {

	@Override
	public WineTypeDTO convert(WineType source) {
		WineTypeDTO eDto = new WineTypeDTO();

		eDto.setId(source.getId());
		eDto.setName(source.getName());

		return eDto;

	}

	public List<WineTypeDTO> convert(List<WineType> wines) {
		List<WineTypeDTO> wineDTOs = new ArrayList<>();

		for (WineType e : wines) {
			wineDTOs.add(convert(e));
		}

		return wineDTOs;
	}

}

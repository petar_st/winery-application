package com.ftninformatika.jwd.modul3.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.modul3.test.model.Wine;

public interface WineService {
	
		List<Wine> findAll();

		Wine findOneById(Long id);

		Wine save(Wine entity);

		Wine update(Wine entity);

		Wine delete(Long id);

		Page<Wine> find(Long wineryId, String wineName, Integer pageNo);
		
		Void processAddToWarehouse(Long vinoId, int numberOfBottles);
		
		Integer processPurchase(Long vinoId, int numberOfBottles);
}
package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import com.ftninformatika.jwd.modul3.test.model.Winery;
import com.ftninformatika.jwd.modul3.test.web.dto.WineryDTO;

@Component
public class WineryToWineryDTO implements Converter<Winery, WineryDTO> {

	@Override
	public WineryDTO convert(Winery source) {
		WineryDTO eDto = new WineryDTO();

		eDto.setId(source.getId());
		eDto.setName(source.getName());
		eDto.setYearOfEstablishment(source.getYearOfEstablishment());

		return eDto;

	}

	public List<WineryDTO> convert(List<Winery> entities) {
		List<WineryDTO> entityDTOs = new ArrayList<>();

		for (Winery e : entities) {
			entityDTOs.add(convert(e));
		}

		return entityDTOs;
	}

}


package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;


import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Address;
import com.ftninformatika.jwd.modul3.test.web.dto.AddressDTO;


@Component
public class AddressToAddressDto implements Converter<Address, AddressDTO> {

    @Override
    public AddressDTO convert(Address address) {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setId(address.getId());
        addressDTO.setBroj(address.getNumber());
        addressDTO.setUlica(address.getStreet());
        return addressDTO;
    }

    public List<AddressDTO> convert(List<Address> addresses){
        List<AddressDTO> addressDTOS = new ArrayList<>();

        for(Address a : addresses) {
            AddressDTO dto = convert(a);
            addressDTOS.add(dto);
        }

        return addressDTOS;
    }

}
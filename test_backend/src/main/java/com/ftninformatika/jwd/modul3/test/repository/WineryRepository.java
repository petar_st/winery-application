package com.ftninformatika.jwd.modul3.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.modul3.test.model.Winery;

@Repository
public interface WineryRepository extends JpaRepository<Winery, Long>{
	Winery findOneById(Long id);
}

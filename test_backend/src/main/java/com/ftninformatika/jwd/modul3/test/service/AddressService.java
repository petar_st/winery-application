package com.ftninformatika.jwd.modul3.test.service;





import java.util.List;
import java.util.Optional;

import com.ftninformatika.jwd.modul3.test.model.Address;

public interface AddressService {

    Optional<Address> findOne(Long id);

    List<Address> findAll();

    Address save(Address address);

    void delete(Long id);

}

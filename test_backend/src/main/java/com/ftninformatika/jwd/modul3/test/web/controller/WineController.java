package com.ftninformatika.jwd.modul3.test.web.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.test.model.Wine;
import com.ftninformatika.jwd.modul3.test.service.WineService;
import com.ftninformatika.jwd.modul3.test.support.WineDtoToWine;
import com.ftninformatika.jwd.modul3.test.support.WineToWineDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.WineDTO;

@RestController
@RequestMapping(value = "/api/wines")
@Validated
public class WineController {

    @Autowired
    private WineService wineService;

    @Autowired
    private WineToWineDTO toWineDTO;

    @Autowired
    private WineDtoToWine toVina;


    @GetMapping
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public ResponseEntity<List<WineDTO>> getAll(
            @RequestParam(required = false) Long wineryId,
            @RequestParam(required = false) String wineName,
            @RequestParam(value = "pageNo", defaultValue = "0") Integer pageNo) {


        Page<Wine> page = wineService.find(wineryId, wineName, pageNo);

        HttpHeaders headers = new HttpHeaders();
        headers.add("total-pages", Integer.toString(page.getTotalPages()));

        return new ResponseEntity<List<WineDTO>>(toWineDTO.convert(page.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    public ResponseEntity<WineDTO> getOne(@PathVariable Long id) {
        Wine wine = wineService.findOneById(id);

        if (wine != null) {
            return new ResponseEntity<>(toWineDTO.convert(wine), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Wine wine = wineService.delete(id);

        if (wine == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<WineDTO> update(@PathVariable Long id, @Valid @RequestBody WineDTO wineDTO) {

        if (!id.equals(wineDTO.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Wine c = toVina.convert(wineDTO);
        Wine cSaved = wineService.update(c);

        return new ResponseEntity<>(toWineDTO.convert(cSaved), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<WineDTO> create(@Valid @RequestBody WineDTO wineDTO) {
        Wine c = toVina.convert(wineDTO);
        Wine cSaved = wineService.save(c);

        return new ResponseEntity<>(toWineDTO.convert(cSaved), HttpStatus.CREATED);
    }

    @GetMapping(value = "/order")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Void> order(@RequestParam Long wineId, @RequestParam int numberOfBottlesToOder) {
        wineService.processAddToWarehouse(wineId, numberOfBottlesToOder);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/buy")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<Void> buy(@RequestParam Long wineId, @RequestParam int numberOfBottlesToBuy) {

        int value = wineService.processPurchase(wineId, numberOfBottlesToBuy);

        if (value == 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}

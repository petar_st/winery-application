package com.ftninformatika.jwd.modul3.test.service;

import java.util.List;

import com.ftninformatika.jwd.modul3.test.model.WineType;

public interface WineTypeService {
	List<WineType> findAll();

	WineType findOneById(Long id);

}

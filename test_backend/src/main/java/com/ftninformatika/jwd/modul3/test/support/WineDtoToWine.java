package com.ftninformatika.jwd.modul3.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Wine;
import com.ftninformatika.jwd.modul3.test.service.WineTypeService;
import com.ftninformatika.jwd.modul3.test.service.WineryService;
import com.ftninformatika.jwd.modul3.test.service.WineService;
import com.ftninformatika.jwd.modul3.test.web.dto.WineDTO;

@Component
public class WineDtoToWine implements Converter<WineDTO, Wine> {

	@Autowired
	private WineService wineService;
	
	@Autowired
	private WineryService wineryService;
	
	@Autowired
	private WineTypeService wineTypeService;

	@Override
	public Wine convert(WineDTO source) {
		Wine entity;

		if (source.getId() == null) {
			entity = new Wine();
		}

		else {
			entity = wineService.findOneById(source.getId());
		}

	entity.setNumberOfAvailableBottles(source.getNumberOfAvailableBottles());
	entity.setPricePerBottle(source.getPricePerBottle());
	entity.setYear(source.getYear());
	entity.setName(source.getName());
	entity.setDescription(source.getDescription());
	entity.setWinery(wineryService.findOneById(source.getWineryId()));
	entity.setWineType(wineTypeService.findOneById(source.getWineTypeId()));
	
		return entity;
	}
}
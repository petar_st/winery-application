package com.ftninformatika.jwd.modul3.test.web.dto;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;

public class WineDTO {
    private Long id;


    private String name;

    @Length(max = 120)
    private String description;

    @Min(value = 0)
    private int year;

    @Min(value = 0)
    private Double pricePerBottle;


    private int numberOfAvailableBottles;


    private Long wineTypeId;
    private String wineTypeName;

    private Long wineryId;
    private String wineryName;

    public WineDTO() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Double getPricePerBottle() {
        return pricePerBottle;
    }

    public void setPricePerBottle(Double pricePerBottle) {
        this.pricePerBottle = pricePerBottle;
    }

    public int getNumberOfAvailableBottles() {
        return numberOfAvailableBottles;
    }

    public void setNumberOfAvailableBottles(int numberOfAvailableBottles) {
        this.numberOfAvailableBottles = numberOfAvailableBottles;
    }

    public Long getWineTypeId() {
        return wineTypeId;
    }

    public void setWineTypeId(Long wineTypeId) {
        this.wineTypeId = wineTypeId;
    }

    public String getWineTypeName() {
        return wineTypeName;
    }

    public void setWineTypeName(String wineTypeName) {
        this.wineTypeName = wineTypeName;
    }

    public Long getWineryId() {
        return wineryId;
    }

    public void setWineryId(Long wineryId) {
        this.wineryId = wineryId;
    }

    public String getWineryName() {
        return wineryName;
    }

    public void setWineryName(String wineryName) {
        this.wineryName = wineryName;
    }
}

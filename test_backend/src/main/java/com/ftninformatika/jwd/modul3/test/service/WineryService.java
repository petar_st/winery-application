package com.ftninformatika.jwd.modul3.test.service;

import java.util.List;

import com.ftninformatika.jwd.modul3.test.model.Winery;

public interface WineryService {
	List<Winery> findAll();
	
	Winery findOneById(Long id);

	Winery save(Winery entity);
}

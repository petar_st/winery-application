package com.ftninformatika.jwd.modul3.test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Wine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotBlank
    @NotNull
    private String name;

    @Column
    private String description;

    @Column
    @NotNull
    private int year;

    @Column
    private Double pricePerBottle;

    @Column
    private int numberOfAvailableBottles;

    @ManyToOne
    private WineType wineType;

    @ManyToOne
    private Winery winery;

    public Wine() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Double getPricePerBottle() {
        return pricePerBottle;
    }

    public void setPricePerBottle(Double pricePerBottle) {
        this.pricePerBottle = pricePerBottle;
    }

    public int getNumberOfAvailableBottles() {
        return numberOfAvailableBottles;
    }

    public void setNumberOfAvailableBottles(int numberOfAvailableBottles) {
        this.numberOfAvailableBottles = numberOfAvailableBottles;
    }

    public WineType getWineType() {
        return wineType;
    }

    public void setWineType(WineType wineType) {
        this.wineType = wineType;
    }

    public Winery getWinery() {
        return winery;
    }

    public void setWinery(Winery winery) {
        this.winery = winery;
    }
}

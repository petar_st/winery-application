package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ftninformatika.jwd.modul3.test.model.Winery;
import com.ftninformatika.jwd.modul3.test.repository.WineryRepository;
import com.ftninformatika.jwd.modul3.test.service.WineryService;
@Service
public class JpaWineryService implements WineryService {
	
	@Autowired
	private WineryRepository wineryRepository;
	
	@Override
	public List<Winery> findAll() {
		List<Winery> entities = wineryRepository.findAll();
		return entities;
	}

	@Override
	public Winery findOneById(Long id) {
		return wineryRepository.findOneById(id);

	}

	@Override
	public Winery save(Winery entity) {
		return wineryRepository.save(entity);
	}




}

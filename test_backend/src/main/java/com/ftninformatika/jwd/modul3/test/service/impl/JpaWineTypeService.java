package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.test.model.WineType;
import com.ftninformatika.jwd.modul3.test.repository.WineTypeRepository;
import com.ftninformatika.jwd.modul3.test.service.WineTypeService;

@Service
public class JpaWineTypeService implements WineTypeService {

	@Autowired
	private WineTypeRepository tipVinaRepository;

	@Override
	public List<WineType> findAll() {
		return tipVinaRepository.findAll();

	}

	@Override
	public WineType findOneById(Long id) {
		return tipVinaRepository.findOneById(id);

	}

}

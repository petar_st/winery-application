package com.ftninformatika.jwd.modul3.test.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.test.model.Address;
import com.ftninformatika.jwd.modul3.test.repository.AddressRepository;
import com.ftninformatika.jwd.modul3.test.service.AddressService;

import java.util.List;
import java.util.Optional;

@Service
public class JpaAddressService implements AddressService {
    @Autowired
    private AddressRepository addressaRepository;

    @Override
    public Optional<Address> findOne(Long id) {
        return addressaRepository.findById(id);
    }

    @Override
    public List<Address> findAll() {
        return addressaRepository.findAll();
    }

    @Override
    public Address save(Address adresa) {
        return addressaRepository.save(adresa);
    }

    @Override
    public void delete(Long id) {
        addressaRepository.deleteById(id);
    }

}

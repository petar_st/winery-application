package com.ftninformatika.jwd.modul3.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.test.model.WineType;
import com.ftninformatika.jwd.modul3.test.service.WineTypeService;
import com.ftninformatika.jwd.modul3.test.support.WineTypeToWineTypeDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.WineTypeDTO;

@RestController
@RequestMapping(value = "/api/wine-types")
@Validated
public class WineTypeController {

	@Autowired
	private WineTypeService wineTypeService;

	@Autowired
		private WineTypeToWineTypeDTO toTipVinaDto;

	@GetMapping
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	public ResponseEntity<List<WineTypeDTO>> getAll() {

		List<WineType> wineTypeList = wineTypeService.findAll();

		return new ResponseEntity<List<WineTypeDTO>>(toTipVinaDto.convert(wineTypeList), HttpStatus.OK);
	}

}

package com.ftninformatika.jwd.modul3.test.support;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Address;
import com.ftninformatika.jwd.modul3.test.service.AddressService;
import com.ftninformatika.jwd.modul3.test.web.dto.AddressDTO;

@Component
public class AddressDtoToAddress implements Converter<AddressDTO, Address> {

    @Autowired
    private AddressService addressService;

    @Override
    public Address convert(AddressDTO addressDTO) {
        Long id = addressDTO.getId();
        Address address = id == null ? new Address() : addressService.findOne(id).get();

        if(address != null) {
            address.setId(id);
            address.setNumber(addressDTO.getBroj());
            address.setStreet(addressDTO.getUlica());
        }

        return address;
    }

}


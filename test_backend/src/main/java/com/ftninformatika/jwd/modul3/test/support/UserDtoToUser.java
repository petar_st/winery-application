package com.ftninformatika.jwd.modul3.test.support;

import com.ftninformatika.jwd.modul3.test.model.User;
import com.ftninformatika.jwd.modul3.test.service.AddressService;
import com.ftninformatika.jwd.modul3.test.service.UserService;
import com.ftninformatika.jwd.modul3.test.web.dto.UserDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


import org.springframework.beans.factory.annotation.Autowired;

@Component
public class UserDtoToUser implements Converter<UserDTO, User> {

    @Autowired
    private UserService userService;

    @Autowired
    private AddressService addressService;

    @Override
    public User convert(UserDTO userDTO) {
        User user = null;
        if (userDTO.getId() != null) {
            user = userService.findOne(userDTO.getId()).get();
        }

        if (user == null) {
            user = new User();
        }

        user.setUsername(userDTO.getUsername());
        user.setAddress(addressService.findOne(userDTO.getAddressDTO().getId()).get());
        user.seteMail(userDTO.geteMail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(user.getLastName());

        return user;
    }

}



package com.ftninformatika.jwd.modul3.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.test.model.Winery;

import com.ftninformatika.jwd.modul3.test.service.WineryService;
import com.ftninformatika.jwd.modul3.test.support.WineTypeToWineTypeDTO;
import com.ftninformatika.jwd.modul3.test.support.WineryToWineryDTO;

import com.ftninformatika.jwd.modul3.test.web.dto.WineryDTO;

@RestController
@RequestMapping(value = "/api/wineries")
@Validated
public class WineryController {

	@Autowired
	private WineryService wineryService;

	@Autowired
	private WineryToWineryDTO toWineryDTO;

	@GetMapping
	@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
	public ResponseEntity<List<WineryDTO>> getAll() {

		List<Winery> wineries = wineryService.findAll();

		return new ResponseEntity<List<WineryDTO>>(toWineryDTO.convert(wineries), HttpStatus.OK);
	}

}
